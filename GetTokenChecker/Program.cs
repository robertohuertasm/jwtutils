﻿using System;
using System.IO;
using System.Net.Http;

namespace GetTokenChecker
{
    class Program
    {
        private const string LATEST_TXT = "latest.txt";
        private const string SEPARATOR = "####";

        static void Main()
        {
            Console.WriteLine("WELCOME TO GETTOKENCHECKER APP *******");
            string url, content;
            var existsLatest = ReadLatest(out url, out content);
            if (existsLatest)
            {
                Console.WriteLine("Do you want to use latest token url? (Y/N)");
                Console.WriteLine(url);
                var r = Console.ReadKey(true);
                if (r.Key != ConsoleKey.Y && r.Key != ConsoleKey.Enter)
                {
                    url = ReadUrl();
                }

                Console.WriteLine("Do you want to use latest request content? (Y/N)");
                Console.WriteLine(content);
                r = Console.ReadKey(true);
                if (r.Key != ConsoleKey.Y && r.Key != ConsoleKey.Enter)
                {
                    content = ReadContent();
                }
            }
            else
            {
                url = ReadUrl();
                content = ReadContent();
            }
            
            StartRequest(url, content);
        }

        private static bool ReadLatest(out string url, out string content)
        {
            url = null;
            content = null;
            if (!File.Exists(LATEST_TXT)) return false;
            var s = File.ReadAllText(LATEST_TXT);
            var ss = s.Split(new[] {SEPARATOR}, StringSplitOptions.RemoveEmptyEntries);
            if (ss.Length != 2) return false;
            url = ss[0];
            content = ss[1];
            return true;
        }

        private static void StartRequest(string url, string content)
        {
            var h = new HttpClientHandler {UseDefaultCredentials = true};
            var c = new HttpClient(h);
            string s="";
            try
            {
                var r = c.PostAsync(url, new StringContent(content)).Result;
                s = r.Content.ReadAsStringAsync().Result;
            }
            catch (AggregateException agEx)
            {
                foreach (var iex in agEx.InnerExceptions)
                {
                    s += $"{iex.Message}\r\n{iex.StackTrace}\r\n";
                    if(iex.InnerException!=null)
                        s += $"{iex.InnerException.Message}\r\n{iex.InnerException.StackTrace}\r\n";
                }
            }
            catch (Exception ex)
            {
                s = $"{ex.Message}\r\n{ex.StackTrace}";
            }
            
            File.WriteAllText(LATEST_TXT, $"{url}{SEPARATOR}{content}");
            Console.WriteLine("RESPONSE:");
            Console.WriteLine("");
            Console.Write(s);
            Console.Read();
        }

        private static string ReadContent()
        {
            while (true)
            {
                Console.WriteLine("Insert the request content (grant_type, username, password & client_id):");
                var content = Console.ReadLine();
                if (content != null
                    && content.StartsWith("grant_type=password")
                    && content.Contains("&username=")
                    && content.Contains("&password=")
                    && content.Contains("&client_id=")) return content;
                Console.WriteLine("The request content was not correctly formatted!!");
                Console.WriteLine("Try again.");
            }
        }

        private static string ReadUrl()
        {
            while (true)
            {
                Console.WriteLine("Insert the Token endpoint (URL):");
                var url = Console.ReadLine();
                Uri uri;
                var ok = Uri.TryCreate(url, UriKind.Absolute, out uri);
                if (ok) return url;
                Console.WriteLine("The entered url was not correctly formatted!!");
                Console.WriteLine("Try again.");
            }
        }
    }
}
