#DOWNLOAD

If you want to download the executable without compiling then you can download it from [here](https://gitlab.com/tokiota/GetTokenChecker/raw/master/GetTokenChecker.zip).

You can also use the website tools at [http://jwtutils.azurewebsites.net/](http://jwtutils.azurewebsites.net/).