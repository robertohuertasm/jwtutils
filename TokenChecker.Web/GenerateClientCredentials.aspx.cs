﻿using System;
using System.Web.UI;

namespace TokenChecker.Web
{
    public partial class GenerateClientCredentials : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            GenerateCredentials();
        }

        private void GenerateCredentials()
        {
            
            txtClientId.Text = Checker.GetClientId();
            txtClientSecret.Text = Checker.GetClientSecret();
        }

        protected void btnSend_OnClick(object sender, EventArgs e)
        {
            GenerateCredentials();
        }
    }
}