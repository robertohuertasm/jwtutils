﻿<%@ Page Title="Check Token Validity" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TokenChecker.Web._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <p>Let's check that the token is valid. Insert the token and the secret (base64)</p>
    <asp:HiddenField runat="server" ID="hdIsOk" ClientIDMode="Static" Value="-1" />
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <h2>JWT Token</h2>
                    <asp:TextBox runat="server" ID="txtToken" TextMode="MultiLine" CssClass="jwt"></asp:TextBox>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <h2>Secret</h2>
                    <asp:TextBox runat="server" ID="txtSecret"></asp:TextBox> 
                    <asp:CheckBox runat="server" ID="chkBase64" Text="Base 64 Encoded" Checked="True" CssClass="base64"/>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-6">
                    <h2>Header</h2>
                    <asp:TextBox runat="server" ID="txtHeader" TextMode="MultiLine" ReadOnly="True"></asp:TextBox>
                </div>
                <div class="col-md-6">
                    <h2>Payload</h2>
                    <asp:TextBox runat="server" ID="txtPayload" TextMode="MultiLine" ReadOnly="True"></asp:TextBox>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Signature</h2>
                    <asp:TextBox runat="server" ID="txtSignature" ReadOnly="True"></asp:TextBox>
                </div>
                
            </div>
            <div class="row">
                <div class="col-md-12">
                    <asp:Button runat="server" ID="btnSend" Text="Validate!" OnClick="btnSend_OnClick" />
                </div>
            </div>

        </div>
    </div>

</asp:Content>
