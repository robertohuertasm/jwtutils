﻿using System;
using System.Web.UI;

namespace TokenChecker.Web
{
    public partial class _Default : Page
    {

        protected void btnSend_OnClick(object sender, EventArgs e)
        {
            var token = txtToken.Text.Split(new[] { "." }, StringSplitOptions.RemoveEmptyEntries);
            txtHeader.Text = Checker.Base64Decode(token[0]);
            txtPayload.Text = Checker.Base64Decode(token[1]);
            txtSignature.Text = token[2];

            var signature = Checker.GenerateSignature(token[0], token[1], txtSecret.Text, chkBase64.Checked);
            hdIsOk.Value = signature == token[2] ? "1" : "0";
        }
    }
}