﻿using System;
using System.Web.UI;

namespace TokenChecker.Web
{
    public partial class GenerateSignature : Page
    {
       
        protected void btnSend_OnClick(object sender, EventArgs e)
        {
            var result = Checker.GenerateSignature(txtHeader.Text, txtPayload.Text, txtSecret.Text, chkBase64.Checked);
            txtSignature.Text = result;
        }
    }
}