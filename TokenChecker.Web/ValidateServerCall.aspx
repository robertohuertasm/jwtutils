﻿<%@ Page Title="Contact" Async="true" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ValidateServerCall.aspx.cs" Inherits="TokenChecker.Web.ValidateServerCall" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <p>Let's check if we can get a token from our OAuth server.</p>

    <div class="row">
        <div class="col-md-6">
            <h2>Token Endpoint (Url)</h2>
            <asp:TextBox runat="server" ID="txtUrl" TextMode="MultiLine"></asp:TextBox>
        </div>
        <div class="col-md-6">
            <h2>Content</h2>
            <asp:TextBox runat="server" ID="txtContent" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:Button runat="server" ID="btnSend" Text="Get Response!" OnClick="btnSend_OnClick" />
            <br />
            <h2>Response</h2>
            <asp:TextBox runat="server" ID="txtResponse" ReadOnly="True" TextMode="MultiLine" ></asp:TextBox>
        </div>
    </div>

</asp:Content>
