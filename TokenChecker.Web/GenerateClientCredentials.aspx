﻿<%@ Page Title="Generate Client Credentials" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GenerateClientCredentials.aspx.cs" Inherits="TokenChecker.Web.GenerateClientCredentials" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <p>Get some random Client Id and Client Secret.</p>

    <div class="row">
        <div class="col-md-6">
            <h2>Client Id</h2>
            <asp:TextBox runat="server" ID="txtClientId"  ReadOnly="True"></asp:TextBox>
        </div>
        <div class="col-md-6">
            <h2>Client Secret</h2>
            <asp:TextBox runat="server" ID="txtClientSecret"  ReadOnly="True"></asp:TextBox>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:Button runat="server" ID="btnSend" Text="Generate New Credentials!" OnClick="btnSend_OnClick" />
        </div>
    </div>
</asp:Content>
