﻿<%@ Page Title="Generate Signature" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GenerateSignature.aspx.cs" Inherits="TokenChecker.Web.GenerateSignature" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <p>Provide header, payload & secret and get a signature.</p>

    <div class="row">
        <div class="col-md-4">
            <h2>Header</h2>
            <asp:TextBox runat="server" ID="txtHeader" TextMode="MultiLine"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <h2>Payload</h2>
            <asp:TextBox runat="server" ID="txtPayload" TextMode="MultiLine"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <h2>Secret</h2>
            <asp:TextBox runat="server" ID="txtSecret" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div>
                <asp:CheckBox runat="server" ID="chkBase64" Checked="True" Text="Secret is Base64 encoded" CssClass="base64" />
            </div>
            <asp:Button runat="server" ID="btnSend" Text="Generate Signature!" OnClick="btnSend_OnClick" />
            <br />
            <h2>Signature</h2>
            <asp:TextBox runat="server" ID="txtSignature" ReadOnly="True" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>



</asp:Content>
