﻿using System;
using System.Web.UI;

namespace TokenChecker.Web
{
    public partial class ValidateServerCall : Page
    {
        protected async void btnSend_OnClick(object sender, EventArgs e)
        {
            var s = await Checker.GetTokenFromRequestAsync(txtUrl.Text, txtContent.Text);
            txtResponse.Text = s;
        }
    }
}