﻿using System;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Security.DataHandler.Encoder;

namespace TokenChecker
{
    public class Checker
    {
        public static string GenerateSignature(string header, string payload, string secret, bool encodedSecret)
        {
            var message = Encoding.UTF8.GetBytes($"{header}.{payload}");
            var key = encodedSecret ? TextEncodings.Base64Url.Decode(secret) : Encoding.UTF8.GetBytes(secret); //secret!
            var h = new HMACSHA256(key);
            h.ComputeHash(message);
            var result = TextEncodings.Base64Url.Encode(h.Hash);
            return result;
        }

        public static string Base64Decode(string str)
        {
            var b = TextEncodings.Base64Url.Decode(str);
            var result = Encoding.UTF8.GetString(b);
            return result;
        }

        public static string GetClientSecret()
        {
            var key = new byte[32];
            RandomNumberGenerator.Create().GetBytes(key);
            var base64Secret = TextEncodings.Base64Url.Encode(key);
            return base64Secret;
        }

        public static string GetClientId()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }

        public static async Task<string> GetTokenFromRequestAsync(string url, string content)
        {
            var h = new HttpClientHandler { UseDefaultCredentials = true };
            var c = new HttpClient(h);
            var s = string.Empty;
            try
            {
                var r = await c.PostAsync(url, new StringContent(content));
                s = await r.Content.ReadAsStringAsync();
            }
            catch (AggregateException agEx)
            {
                foreach (var iex in agEx.InnerExceptions)
                {
                    s += $"{iex.Message}\r\n{iex.StackTrace}\r\n";
                    if (iex.InnerException != null)
                        s += $"{iex.InnerException.Message}\r\n{iex.InnerException.StackTrace}\r\n";
                }
            }
            catch (Exception ex)
            {
                s = $"{ex.Message}\r\n{ex.StackTrace}";
            }

            return s;
        }
    }
}
